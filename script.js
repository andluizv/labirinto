// Variáveis globais
const mapsStore ={'choice1': [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
], 'choice2': [
    "WWWWWWWWWWWWWFW",
    "W           W W",
    "W WWWWWWWWW W W",
    "W W       W W W",
    "W W WWWWW W W W",
    "W W W   W W W W",
    "W W W WSW W W W",
    "W W W WWW W W W",
    "W W W     W W W",
    "W W WWWWWWW W W",
    "W W         W W",
    "W WWWWWWWWWWW W",
    "W             W",
    "WWWWWWWWWWWWWWW",
], 'choice3': [
    "WWWWW", "S   F", "WWWWW" 
] }
let createdSave = [];
const gameContainer = document.querySelector('.game-container');
const aside = document.querySelector('aside');
const newWidth = document.querySelector('#new-width');
const newHeight = document.querySelector('#new-height');
const toolsMenu = document.querySelector('.creator-container');
const widthInput = document.querySelector('#new-width');
const heightInput = document.querySelector('#new-height');
const creatorBtn = document.querySelector('#creator');
const newPlayBtn = document.querySelector('#new-play');
let player = '';

// Funções de criação
const createWall = () => {
    const wall = document.createElement('div');
    wall.classList.add('wall');
    gameContainer.appendChild(wall);
}
const createPath = () => {
    const path = document.createElement('div');
    path.classList.add('path');
    path.classList.add('center');
    gameContainer.appendChild(path);
}
const createStart = () => {
    const start = document.createElement('div');
    start.classList.add('start');
    start.classList.add('center');
    gameContainer.appendChild(start);
}
const createFinish = () => {
    const finish = document.createElement('div');
    finish.classList.add('finish');
    finish.classList.add('center');
    gameContainer.appendChild(finish);
}
const createPlayer = () => {
    const newplayer = document.createElement('div');
    const start = document.querySelector('.start');
    newplayer.classList.add('player');
    newplayer.innerHTML = '<i class="fas fa-ghost"></i>'
    start.appendChild(newplayer);
    player = document.querySelector('.player');
}
//funções para ajustar tamanho do mapa
const getMapWidth = (scheme) => {
    //verificar padrões
    scheme.forEach(item => {
        if (item.length === scheme[0].length){
            return;
        }
        alert(`Há linhas desalinhadas, todas as linhas devem ter o mesmo tamanho! Ver linha: ${scheme.indexOf(item)}`);
        return;
    });
    
    return scheme[0].length;
}
const getMapHeight = (scheme) => {
    return scheme.length;
}
const widthAjust = (scheme) => {
    gameContainer.style.width = getMapWidth(scheme)*2 + 'vw';
}

//renderizar o mapa
const mapBuild = (scheme) => {
    widthAjust(scheme);
    scheme.forEach(line =>{
        let splited = line.split('');
        splited.forEach(item => {
            if (item === 'W'){
                return createWall();
            }
            if (item === ' '){
                return createPath();
            }
            if (item === 'S'){
                return createStart();
            }
            if (item === 'F'){
                return createFinish();
            }
            document.body.innerHTML = '';
            alert(`Tem um erro no mapa informado, provalemnte um caractere não reconhecido, verifique por: ${item}`);
        })
    })
    if (document.querySelector('.start') == null){
        return;
    }
createPlayer();
}


//Captura elemento acima e abaixo do jogador no mapa
const getWayUp = (scheme) => {
    const playerParent = player.parentElement;
    const allPositions = Array.from(playerParent.parentNode.children);
    let wayUp = allPositions[allPositions.indexOf(playerParent) - getMapWidth(scheme)];
    if (document.querySelector('input[name="choices"]:checked').id === "create") {
        wayUp = allPositions[allPositions.indexOf(playerParent) - getNewMapWidth(scheme)];
    }
    return wayUp;
}
const getWayDown = (scheme) => {
    const playerParent = player.parentElement;
    const allPositions = Array.from(playerParent.parentNode.children);
    let wayDown = allPositions[allPositions.indexOf(playerParent) + getMapWidth(scheme)];
    if (document.querySelector('input[name="choices"]:checked').id === "create") {
        wayUp = allPositions[allPositions.indexOf(playerParent) + getNewMapWidth(scheme)];
    }
    return wayDown;
}
//Verifica e mostra vitória
const checkVictory = () => {
    if (player.parentElement.classList.contains('finish')){
        return true;
    }
}
const showVictory = (scheme) => {
    
    const victoryModal = document.createElement('div');
    victoryModal.classList.add('modal');
    victoryModal.style.cssText = `width: ${getMapWidth(scheme)*2}vw; height: ${getMapHeight(scheme)*2}vw; font-size: ${getMapHeight(scheme)/4}vw;`
    if (document.querySelector('input[name="choices"]:checked').id === "create"){
        victoryModal.style.cssText = `width: ${getNewMapWidth(scheme)*2}vw; height: ${getNewMapHeight(scheme)*2}vw; font-size: ${getMapHeight(scheme)/4}vw;`
    }
    victoryModal.innerText = 'Vitória!\n Agora vai tomar um café vai...'
    gameContainer.appendChild(victoryModal);
}
//Movimentação
const movePlayer = (direction, map) => {
    if (checkVictory()){
        return;
    }
    if (direction  === 'ArrowRight'){
        let prox = player.parentElement.nextElementSibling;
        let actual = player.parentElement;
        if (prox.classList.contains('wall') || (prox.classList.contains('finish') && actual.classList.contains('start'))){
            return;
        }
        prox.appendChild(player)
    }
    if (direction === 'ArrowLeft'){
        let ant = player.parentElement.previousElementSibling;
        let actual = player.parentElement;
        if (ant.classList.contains('wall') || (ant.classList.contains('finish') && actual.classList.contains('start'))){
            return;
        }
        ant.appendChild(player);
    }
    if (direction === 'ArrowUp'){
        let toGoUp = getWayUp(map);
        if (toGoUp.classList.contains('wall')){
            return;
        }
        toGoUp.appendChild(player);
    }
    if (direction === 'ArrowDown'){
        let toGoDown = getWayDown(map);
        if (toGoDown.classList.contains('wall')){
            return;
        }
        toGoDown.appendChild(player);
    }
    if (checkVictory()){
        showVictory(map);
    }
}

//Pre evento, captura do aside
let asideChecked = 'choice1';
const options = document.querySelector('input[name="choices"]:checked').id;
const getChoice = () => {
    return document.querySelector('input[name="choices"]:checked').id;
}

//Sistema criador de mapa
const getNewMapWidth = () => {
    return widthInput.value;
}
const getNewMapHeight = () => {
    return heightInput.value;
}
const ajustNewMapwidth = () => {
    gameContainer.style.width = getNewMapWidth() * 2 + 'vw';
}
const newMapSchemeGenerator = (width, height) => {
    let option = document.querySelector('input[name="map-type"]:checked').id;
    let output = [];
    if (option === 'empty-map'){
        output.push('W'.padEnd(width, 'W'));
        for (let i = 0; i < height - 2; i++){
            output.push(`W${' '.padEnd(width - 2, ' ')}W`)
        }
        output.push('W'.padEnd(width, 'W'));
        createdSave = output;
        return output;
    };
    
    for (let i = 0; i < height; i++){
        output.push('W'.padEnd(width, 'W'));
    }
    createdSave = output;   
    return output;
}
const generator = () => {
    gameContainer.innerHTML = '';
    let mapWidth = getNewMapWidth();
    let mapHeight = getNewMapHeight();
    ajustNewMapwidth();
    let newMap = newMapSchemeGenerator(mapWidth,mapHeight);
    mapBuild(newMap);
}
const drawStart = (ref) => {
    let older = document.querySelector('.start');
    if (older !== null){
        older.classList.add('wall');
        older.classList.remove('start');
    }
    
    ref.classList.remove(...ref.classList);
    ref.classList.add('start');
}
const drawFinish = (ref) => {
    let older = document.querySelector('.finish');
    if (older !== null){
        older.classList.add('wall');
        older.classList.remove('finish');
    }
    
    ref.classList.remove(...ref.classList);
    ref.classList.add('finish');
}
const drawWall = (ref) => {
    ref.classList.remove(...ref.classList);
    ref.classList.add('wall');
}
const drawPath = (ref) => {
    ref.classList.remove(...ref.classList);
    ref.classList.add('path');
}
const pencil = () => {
    if (document.querySelector('input[name="choices"]:checked').id === 'create'){
        let selection = document.querySelector('input[name="blocks"]:checked').id;
        if (selection === "block-start"){
            return drawStart(event.target);
        } 
        if (selection === "block-finish"){
            return drawFinish(event.target);
        }
        if (selection === "block-wall"){
            return drawWall(event.target);
        }
        if (selection === "block-way"){
            return drawPath(event.target);
        }
    }
}
//eventos
mapBuild(mapsStore[getChoice()]);
window.addEventListener('keydown', (event) => {
    
    let direction = event.key;
    if (document.querySelector('input[name="choices"]:checked').id === "create") {
        return movePlayer(direction, createdSave);
    }
    movePlayer(direction, mapsStore[getChoice()]);
});
aside.addEventListener('click', () => {
    let mapToLoad = getChoice();
    if(mapToLoad === 'create'){
        toolsMenu.classList.remove('--hidden');
        gameContainer.innerHTML = '';
        return;
    }
    toolsMenu.classList.add('--hidden');
    gameContainer.innerHTML = '';
    mapBuild(mapsStore[mapToLoad]);
})
creatorBtn.addEventListener('click', () => {
    generator();
})

gameContainer.addEventListener('mousedown', (event) => {
    pencil();
    this.addEventListener('mousemove', pencil)
    
})
gameContainer.addEventListener('mouseup', () => {
    this.removeEventListener('mousemove', pencil);
})
newPlayBtn.addEventListener('click', () => {
    createPlayer();
})