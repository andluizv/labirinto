# Labirinto

O desafio era construir um simples labirinto com um caminho já projetado em forma de um array... Me empolguei com a idéia de criar o labirinto da forma que eu quisesse...

Projeto em HTML, CSS e JavaScript.

Link para acessar: https://andluizv.gitlab.io/labirinto/
